using CrossTemporalDecoding
using Test
using LinearAlgebra
using StatsBase
using Statistics
using Random

@testset "Gaussian discriminant" begin
	μ = [[-0.5, 0.0],[0.0, 0.5]]
	Σ = Matrix(Diagonal([0.1, 0.1]))
	class_label = [2, 1, 1, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 2, 2, 1, 2, 2, 1, 1, 1, 2, 2, 1, 2, 2, 2, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 2, 1, 2, 2, 2, 2, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 1, 1, 2, 1, 2, 2, 2, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 2, 2]
	X = [-0.515450355110076 -0.6416778550134606 -0.49835510653321663 0.3587975080853628 -0.7489643234555223 0.29341548842374765 -0.35375085595430306 -0.7514156813678459 -0.5489209167295542 -1.0308444369784149 0.16879186535735802 -0.030394858708158744 -0.8738417955616647 -0.0003145686064595828 -0.49726492811070966 -0.3034087355972577 -0.020477803428568423 -0.15453894129878176 -0.05484407491241805 -0.1941225765579664 -0.8734314740559446 -0.18307686011439028 0.04007171706158457 -0.09718029817730478 -0.3097593478959951 0.30110078546198704 0.20684538893631008 -0.4272432871567811 -0.5278428313088718 -0.4494972702881281 -0.4105439972888327 -0.42100593437741934 -0.8246620082955431 0.06739262339980331 -0.5142149474554751 -0.3020250509445593 0.0643614641076462 -0.35819208823692233 -0.10294648801872615 -0.4838856577571255 -0.5431876683102794 -0.554581095820344 -0.16744013864589005 -0.38084388026093685 -0.4642652246375312 0.6759494903117187 0.19202595602425787 -0.7071916426169043 -0.3893195608714752 -1.0254518983899872 0.03257931079766657 -0.5481851518084763 0.22111046545854646 -0.16565427613084727 0.16666043937950062 -0.2158827218170745 -0.7522197626350879 -0.521261414582474 -0.050077479360410244 -0.1110334185714894 0.07206272170902188 -0.23990433063365468 0.4096870421014305 -0.21879601013390293 0.27886233678319705 0.005667188087751804 -0.5621626855996947 -0.6699226792837422 -0.9929090022406749 -0.7701788752588159 0.2472168860356098 -0.9219550246637784 -0.025530484698860538 -0.4734383662565115 -0.3449415996103821 0.2643372818599229 -0.8077978664334368 -0.3559030966608092 -0.9835046673778776 -0.6616022089733906 -0.8824154695006867 -0.287959335467128 0.41480896704918174 -0.5464033345725562 -0.49205954041907146 -0.9394832172745636 -0.04839746725144717 0.0055280459908695736 -0.2376819855545136 -0.16220814356582863 0.1396516490512835 -0.7197555505108532 0.023903497575566646 0.23453454784718086 -0.3466977666225488 -0.06014307999209269 -0.13540120620087068 0.09205764976604641 0.2086729208818253 -0.005245260109657017; 0.6796981146543379 -0.3242666849129433 0.1372353941751512 0.5529773864450878 -0.13965029252372868 1.082782933483338 0.5593703532090447 0.11526369325204693 -0.022120640417853388 0.18252772511128357 0.44802218709155384 0.41852801853445154 -0.2008916038004632 0.32166676111043024 0.2347678410138673 0.38633605826245937 0.260091474853263 0.3399177948200823 0.33892396952183945 0.6890219905174269 -0.4041895569055386 -0.14069794550552417 0.483032434705464 0.41765965958427204 -0.05411827227307631 0.8812934051331236 0.6233728063899275 -0.5222259901482375 0.6302362388457479 0.422669791318388 0.7414200502809538 -0.1332827946466327 0.03305462406179014 0.5071489404444618 0.7092554215701519 0.6418644883034588 -0.023713866122843184 0.2877006615974098 0.40944236749499463 -0.25466426630996114 -0.09073901696990634 -0.3830438446516403 0.3862992370072715 1.0256107507935779 0.3338140237803946 -0.061786571540231816 1.0057158839030895 0.7343858205896919 -0.03818280622417862 -0.11448996020294341 0.8843751599790003 0.4685479196816515 1.030711596732708 0.383677791342308 0.4861528623115214 0.39410681273716835 -0.5334343265393394 -0.10354367819460072 0.04839487714112145 0.34368197653908655 0.825923673096735 0.7714990511145782 0.7216403657275787 0.6380212768776921 0.7299085328335306 0.23814429132107973 -0.04523716876903597 0.8991268486790387 -0.281624680626429 -0.28878268008255387 0.2779799460517624 0.6642839648546306 0.4630082476242283 0.024674066947535007 0.7617221289753446 1.2259449711216663 0.10269699305880278 -0.23978336122199367 0.06930726958202797 0.7286910874966146 0.03968549486667214 0.8375229677898668 0.9049527692971737 0.7295273756816321 -0.6391568874708238 0.024585713602624622 0.34304268132283233 0.42971650771929765 0.05080778240878279 -0.17794048158832784 0.8918351574161363 0.2282854327778352 -0.10427052097487068 0.44089079440083767 0.2034331391935134 0.9448914485232313 -0.12098091304377845 0.2845704376202254 0.4075644579550365 0.920608300679838]
	q = StatsBase.fit(CrossTemporalDecoding.GaussianDiscriminant, 2, X, class_label)
	@test q.pmeans ≈ [-0.5429576231952652 -0.07824241946810832; -0.009649211104915963 0.5764446972067844]
	@test q.Σ ≈ [0.14863727393209022 0.07405168932557506; 0.07405168932557506 0.17393449627082336]
end

@testset "split_trials" begin
	offsets = CrossTemporalDecoding.split_set([1,2,3,4,5], [1//2, 1//2])
	@test offsets == [2,3]
	offsets = CrossTemporalDecoding.split_set([1,2,3,4], [1//2, 1//2])
	@test offsets == [2,2]
	labels = [[1,2,3,4],[1,2,3,4],[3,4,1,4]]
	new_labels = CrossTemporalDecoding.split_trials(labels)
	ll = map(length,new_labels)
	@test ll == [2,2,2]
	@test sort(cat(new_labels[1]...,dims=1)) == labels[1]
	@test sort(cat(new_labels[2]...,dims=1)) == labels[2]
	@test sort(cat(new_labels[3]...,dims=1)) == [1,3,4,4] 
end

@testset "TransformX" begin
	X = rand(3,4,5)
	Y = fill(0.0, 3,4*5)
	CrossTemporalDecoding.transformX!(Y,X)
	@test Y ≈ permutedims(reshape(permutedims(X, [3,2,1]), size(X,3)*size(X,2), size(X,1)),[2,1])
end

@testset "Confusion" begin
	actual = [1,1,2,2,3,3,4,4]
	decoded = fill(0, length(actual), 1,1)
	decoded[:,1,1] .= [1,2,2,3,3,2,4,3]
	cm = CrossTemporalDecoding.get_confusion_matrix(actual, decoded)
	@test cm[1,1,1,1] == 1
	@test cm[2,2,1,1] == 1
	@test cm[3,3,1,1] == 1
	@test cm[4,4,1,1] == 1
	@test cm[1,2,1,1] == 1
	@test cm[1,3,1,1] == 0
	@test cm[1,4,1,1] == 0
	@test cm[2,1,1,1] == 0
	@test cm[2,3,1,1] == 1
	@test cm[2,4,1,1] == 0
	@test cm[3,1,1,1] == 0
	@test cm[3,2,1,1] == 1
	@test cm[3,4,1,1] == 0
	@test cm[4,1,1,1] == 0
	@test cm[4,2,1,1] == 0
	@test cm[4,3,1,1] == 1
end

@testset "Basic" begin
	RNG = MersenneTwister(1234)
	ncells = 20
	nstim = 3
	μ = exp.(randn(RNG, nstim)) #coding matrix
	W = randn(RNG, ncells, nstim) #mixing matrix
	q,r = qr(randn(RNG, nstim,nstim))
	A = q' - q # skew symmetric matrix
	R  = exp(A)
	μ2 = R*μ
	Σ = Matrix(Diagonal(fill(0.05, ncells))) #independent noise

	#simulation
	ntrials = 100
	nbins1 = 10
	nbins2 = 10
	nbins = nbins1 + nbins2
	label = fill(0,ntrials)
	label2 = fill(0,ntrials)
	X = fill(0.0, nbins, ntrials, ncells)
	X2 = fill(0.0, nbins, ntrials, ncells)
	for i in 1:ntrials
		ll = rand(RNG, 1:3)
		label[i] = ll
		_μ = μ[ll:ll]
		for j in 1:nbins1
			X[j,i,:] .= dropdims(W[:,ll:ll]*μ[ll:ll] .+ Σ*randn(RNG, ncells,1),dims=2)
		end
		for j in (nbins1+1):nbins
			X[j,i,:] .= dropdims(W[:,ll:ll]*μ2[ll:ll] .+ Σ*randn(RNG, ncells,1),dims=2)
		end
	end
	@test CrossTemporalDecoding.MultivariateStats.MulticlassLDA{Float64} <: CrossTemporalDecoding.AllDecoders{Float64}
	@test CrossTemporalDecoding.MultivariateStats.MulticlassLDA <: CrossTemporalDecoding.AllDecoders
	Xp = permutedims(X, [3,2,1])
	decoded_label, decoder = CrossTemporalDecoding.decode(Xp,label)
    Q = dropdims(sum(decoded_label .== label,dims=1),dims=1)

	@test Q == [100 100 100 100 100 100 100 100 100 100 28 24 0 32 0 0 0 32 25 5;
				100 100 100 100 100 100 100 100 100 100 29 28 0 32 0 0 0 32 28 10;
				100 100 100 100 100 100 100 100 100 100 29 27 0 32 0 0 0 32 27 7;
				100 100 100 100 100 100 100 100 100 100 28 27 0 32 0 0 0 32 28 8;
				100 100 100 100 100 100 100 100 100 100 30 28 0 32 0 0 0 32 30 7;
				100 100 100 100 100 100 100 100 100 100 32 26 0 32 0 0 0 32 29 7;
				100 100 100 100 100 100 100 100 100 100 29 26 0 32 0 0 0 32 28 9;
				100 100 100 100 100 100 100 100 100 100 29 25 0 32 0 0 0 32 27 9;
				100 100 100 100 100 100 100 100 100 100 29 25 0 32 0 0 0 32 25 12;
				100 100 100 100 100 100 100 100 100 100 28 24 0 32 0 0 0 32 24 2;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100;
				33 33 33 33 33 33 33 33 33 33 100 100 100 100 100 100 100 100 100 100]

	perf,conf_matrix,decoders = CrossTemporalDecoding.run_decoder(Xp,label,nruns=10,RNG=RNG)
	Z = CrossTemporalDecoding.transform(decoders[1], Xp[:,1,1])
	@test length(Z) == nbins
	@test length(Z[1]) == nstim-1

	Zf = CrossTemporalDecoding.transform(decoders[1], Xp)
	@test length(Zf) == nbins
	@test size(Zf[1]) == (nstim-1, ntrials, nbins)

	mperf = dropdims(mean(perf,dims=3),dims=3)
	@test perf[10,10,1] ≈ 1.0
	@test conf_matrix[:,:,10,10] ≈ [72.0 0.0 0.0; 0.0 66.0 0.0; 0.0 0.0 62.0]
	@test  perf[5,15,1] ≈ 0.35000000000000003
	@test conf_matrix[:,:,5,15] ≈ [0.0 0.0 72.0; 0.0 0.0 66.0; 46.0 0.0 16.0]
	@test mperf[5,15] ≈ 16/(72+66+46+16)
	@test conf_matrix[:,:,15,5] ≈ [0.0 17.0 55.0; 0.0 66.0 0.0; 0.0 62.0 0.0]
	@test mperf[15,5] ≈ 66/(17+55+62+66)
	
	#same code for different target
	for i in 1:ntrials
		ll2 = rand(RNG, 1:3)
		ll = label[i]
		label2[i] = ll2
		for j in 1:nbins1
			X2[j,i,:] .= dropdims(W[:,ll:ll]*μ[ll:ll] .+ Σ*randn(RNG, ncells,1),dims=2)
		end
		for j in (nbins1+1):nbins
			X2[j,i,:] .= dropdims(W[:,ll2:ll2]*μ[ll2:ll2] .+ Σ*randn(RNG, ncells,1),dims=2)
		end
	end
	Xp2 = permutedims(X2, [3,2,1])
	decoded_label, decoder = CrossTemporalDecoding.decode(Xp2,label)
    Q1 = dropdims(sum(decoded_label .== label,dims=1),dims=1)
    Q2 = dropdims(sum(decoded_label .== label2,dims=1),dims=1)
	
	#testing on the first target works well for the first period, but is at chance for the second period
	@test Q1 == [100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40;
				 28 28 28 28 28 28 28 28 28 28 40 40 40 40 40 40 40 40 40 40]

	#testing on the second target gives chance performance in the first period,
	#near 100% performance in the second period
	@test Q2 == [28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 28 28 28 28 28 28 28 28 28 28 32 32 32 32 32 32 32 32 32 32;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
				 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0]

	perf,conf_matrix = CrossTemporalDecoding.run_decoder(Xp2,label,label2,nruns=10,RNG=RNG)
	mperf = dropdims(mean(perf,dims=3),dims=3)
	# training using label1 and testing for label2 in the first period gives chance peformance
	@test mperf[5,5] ≈ 0.28
	# training for label1 and testing for label2 in the second period gives near zero performance
	# because label1 is no longer valid
	@test mperf[15,15] ≈ 0.05
	# trainin for label1 in the first period and testing for label2 in the second period gives
	# high performance
	@test mperf[15,5] ≈ 1.0

	@testset "Gaussian discriminant decoder" begin
		decoded_label, decoder = CrossTemporalDecoding.decode(Xp2,label,decoder=CrossTemporalDecoding.GaussianDiscriminant)
		Q1 = dropdims(sum(decoded_label .== label,dims=1),dims=1)
		@test Q1 == [100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 100 100 100 100 100 100 100 100 100 100 0 0 0 0 0 0 0 0 0 0;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39;
					 28 28 28 28 28 28 28 28 28 28 39 39 39 39 39 39 39 39 39 39]
	end
	@testset "Pseudo" begin
		new_label = [label for i in 1:size(Xp2,1)]
		min_ntrials = minimum(map(length, new_label))
		@test min_ntrials == 100
		gidx = [collect(1:length(l)) for l in new_label]
		Y = fill(0.0, size(X2, 1), min_ntrials, size(X2,3))	
		tidx,olabel = CrossTemporalDecoding.sample_trials!(Y, X2, new_label, gidx)
		Yp2 = permutedims(Y, [3,2,1])
		decoded_label, decoder = CrossTemporalDecoding.decode(Yp2,olabel)
		Q3 = dropdims(sum(decoded_label .== olabel,dims=1),dims=1)
		t_values = true
		t_labels = true
		for t in 1:size(tidx,2)
			for i in 1:size(tidx,1)
				t_values = t_values && Y[:,t,i] ≈ X2[:, tidx[i,t], i]
				t_labels = t_labels && new_label[i][tidx[i,t]] == olabel[t]
			end
		end
		@test t_values
		@test t_labels
		perf,conf_matrix,decoders = CrossTemporalDecoding.run_decoder(Xp2[1:end-1,:,:],new_label,nruns=500,RNG=RNG)
		mean_perf = dropdims(mean(perf, dims=3), dims=3)
		@test sum(mean_perf[1:10,1:10])./sum(mean_perf[11:end, 11:end]) > 3.0
		perf,conf_matrix, decoders = CrossTemporalDecoding.run_decoder(Xp2[1:end-1,:,:],new_label,nruns=500,RNG=RNG,numcells=15)
		mean_perf = dropdims(mean(perf, dims=3), dims=3)
		@test sum(mean_perf[1:10,1:10])./sum(mean_perf[11:end, 11:end]) > 3.0
		perf,conf_matrix, decoders = CrossTemporalDecoding.run_decoder(Xp2[1:end-1,:,:],new_label,nruns=500,RNG=RNG,numcells=15, decoder=CrossTemporalDecoding.GaussianDiscriminant)
		mean_perf = dropdims(mean(perf, dims=3), dims=3)
		@test sum(mean_perf[1:10,1:10])./sum(mean_perf[11:end, 11:end]) > 3.0
		perf,conf_matrix, decoders = CrossTemporalDecoding.run_decoder(Xp2[1:end-1,:,:],new_label,nruns=500,RNG=RNG,numcells=15, fixedcells=1:5, decoder=CrossTemporalDecoding.GaussianDiscriminant)
		@test sum(mean_perf[1:10,1:10])./sum(mean_perf[11:end, 11:end]) > 3.0

        Yp,train_label, test_label = CrossTemporalDecoding.sample_trials(Xp2, new_label,ntrain=10,ntest=1, RNG=RNG)
        @test train_label == [2, 1, 1, 3, 2, 2, 1, 1, 1, 1]
        @test test_label == [2]
        @test size(Yp) == (20,11,20)
	end
end

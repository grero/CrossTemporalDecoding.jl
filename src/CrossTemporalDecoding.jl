module CrossTemporalDecoding
using LinearAlgebra
using Random
using MultivariateStats
using Statistics
using Distances
using ProgressMeter

include("mvgaussian.jl")
include("types.jl")

"""
Flattens the last two dimension of `X`, storing the result in `Y`.
"""
function transformX!(Y::Matrix{T}, X::Array{T,3}) where T <: Real
	ncells,ntrials,nbins = size(X)
	for k in 1:nbins
		for j in 1:ntrials
			Y[:,(j-1)*nbins+k] = X[:,j,k]
		end
	end
end

function decode(X::Array{T,3}, label;trainidx=1:size(X,2), testidx=trainidx,
				decoder::Type{T2}=MultivariateStats.MulticlassLDA) where T2 <: AllDecoders where T <: Real
    ncells, ntrials, nbins = size(X)
    # 1) dimensionality reduction
	Xf = fill(zero(T), ncells, ntrials*nbins)
    transformX!(Xf,X)
    pca = fit(MultivariateStats.PCA, Xf)
    #@show pca
    decode(X,label, pca, decoder;trainidx=trainidx, testidx=testidx)
end

function decode(X::Array{T1, 3},label::Vector{T}, pca, decoder::Type{T2}=MultivariateStats.MulticlassLDA;trainidx=1:size(X,2), testidx=trainidx) where T <: Any where T2 <: AllDecoders where T1 <: Real
    ncells, ntrials, nbins = size(X)
	nstim = maximum(label)
    ntrain = length(trainidx)
    ntest = length(testidx)
	nstim = maximum(label)
	Yp = fill(zero(T1), MultivariateStats.outdim(pca), ntrials,nbins)
    for i = 1:nbins
        Yp[:,:,i] .= MultivariateStats.transform(pca, X[:,:,i])
    end
    Yp_train = Yp[:,trainidx,:]
    Yp_test = Yp[:,testidx,:]
    decoded_label = fill(0, ntest, nbins,nbins)
    D = fill(0.0, nstim, length(testidx))
    trainlabel = label[trainidx]
	ldas = Vector{decoder{T1}}(undef, nbins)
    for j1 in 1:nbins
        lda = fit(decoder, nstim, Yp_train[:,:,j1], trainlabel)
        for j2 in 1:nbins
			decode!(view(decoded_label, :,j2,j1), lda, view(Yp_test, :,:,j2))
        end
		ldas[j1] = lda
    end
	decoded_label, Decoders(pca, ldas)
end

function decode!(decoded_label::AbstractVector{T2}, lda, Yp_test::AbstractMatrix{T}) where T <: Real where T2 <: Real
	Q = MultivariateStats.transform(lda, Yp_test)
	ntest = size(decoded_label,1)
	nstim = size(lda.pmeans,2)
	#decode
	dist = Euclidean()
	for k in 1:ntest
		mv = Inf
		mi = 0
		for k2 in 1:nstim
			dkk = Distances.evaluate(dist, lda.pmeans[:,k2], Q[:,k])
			if dkk < mv
				mv = dkk
				mi = k2
			end
		end
		decoded_label[k] = mi
	end
end

function get_confusion_matrix(actual_label::Vector{T}, decoded_label::Array{T,3}) where T <: Integer where T2 <: Real
	ntrials, nbins,_ = size(decoded_label)
	nlabel = max(maximum(actual_label), maximum(decoded_label))
	confusion = fill(0,nlabel,nlabel, nbins, nbins)
	get_confusion_matrix!(confusion, actual_label, decoded_label)
end

function get_confusion_matrix!(confusion::Array{T2,4}, actual_label::Vector{T}, decoded_label::Array{T,3}) where T <: Integer where T2 <: Real
	ntrials, nbins,_ = size(decoded_label)
	for j1 in 1:nbins
		for j2 in 1:nbins
			for t in 1:ntrials
				confusion[actual_label[t], decoded_label[t,j2,j1],j2,j1] += 1.0
			end
		end
	end
	confusion
end

function run_decoder(X::Array{T,3},label::Vector{Int64},testlabel::Vector{Int64}=label;rtrain=0.8, nruns=1000, decoder=MultivariateStats.MulticlassLDA, RNG=MersenneTwister(rand(UInt64))) where T <: Real
    ncells, ntrials, nbins = size(X)
    ntrain = round(Int64, rtrain*ntrials)
    perf = fill(0.0, nbins, nbins, nruns)
	nlabel = maximum(label)
	conf_matrix = fill(0.0, nlabel, nlabel, nbins, nbins)
	prog = Progress(nruns, 1.0, "Training decoders...")
	decoders = Vector{Decoders{T,decoder{T}}}(undef, nruns)
    Threads.@threads for i in 1:nruns
        trainidx = shuffle(RNG, 1:ntrials)[1:ntrain]
        testidx = setdiff(1:ntrials, trainidx)
		dl, decoders[i] = decode(X, label, trainidx=trainidx, testidx=testidx)
        perf[:,:,i] = dropdims(mean(testlabel[testidx].==dl,dims=1),dims=1)
		pm = maximum(mean(perf[:,:,1:i],dims=3))
		get_confusion_matrix!(conf_matrix, testlabel[testidx], dl)
		next!(prog; showvalues=[(:max_perf, pm)])
    end
    perf, conf_matrix, decoders
end


function split_set(idx::Vector{T}, prop::Vector{Rational{T3}}) where T where T3 <: Integer
	offsets = fill(0, length(prop))
	ll = 0
	for (jj,q) in enumerate(prop)
		ddr = q*(length(idx)-ll)
		if isinteger(ddr)
			dd = Int64(ddr) + ll
		else
			dd = Int64(floor(ddr))
			ll = rem(ddr.num, ddr.den)
		end
		offsets[jj] = dd
	end
	offsets
end
"""
Splits the trials represented by `labels` into random subsets with the specified proportions
"""
function split_trials(labels::Vector{Vector{T2}},prop::Vector{Rational{T3}}=[1//2, 1//2];RNG=MersenneTwister(rand(UInt32))) where T2 where T3 <: Integer
	n = length(prop)
	out_labels = Vector{Vector{T2}}[]
	for (ii,label) in enumerate(labels)
		idx = shuffle(RNG, 1:length(label))
		olabel = Vector{T2}[]
		offsets = split_set(label, prop)
		offset = 0
		for dd in offsets
			push!(olabel, label[idx[offset+1:offset+dd]])
			offset += dd
		end
		push!(out_labels, olabel)
	end
	out_labels
end

"""
    sample_trials(X::Array{T,3}, label::Vector{T2}, testlabel=label;RNG=MersenneTwister(rand(UInt32)),
                                                                         ntrain=1500,
                                                                         ntest=100) where T <: Real where T2 <: Vector{T3} where T3

Sample `length(label) length(testlabel)` from the population count matrxi `X` by sampling trials with matching
labels for all cells independetly.
"""
function sample_trials(X::Array{T,3}, label::Vector{T2}, testlabel=label;RNG=MersenneTwister(rand(UInt32)),
                                                                         ntrain=1500,
                                                                         ntest=100) where T <: Real where T2 <: Vector{T3} where T3
    ncells,ntrials,nbins = size(X)
    traintestidx = split_trials([collect(1:length(l)) for l in label];RNG=RNG)
    trainidx = [idx[1] for idx in traintestidx]
    testidx = [idx[2] for idx in traintestidx]
    Xp = permutedims(X, [3,2,1])
    Y = fill(0.0, nbins, ntrain+ntest, ncells)
    _trainidx, trainlabel = sample_trials!(view(Y, :, 1:ntrain,:), Xp, label, trainidx, RNG=RNG)
    _testidx, testlabel = sample_trials!(view(Y, :, (ntrain+1):(ntrain+ntest),:), Xp, testlabel, testidx, RNG=RNG)
    Y, trainlabel, testlabel
end

function sample_trials!(Y::AbstractArray{T,3}, X::AbstractArray{T,3},labels::Vector{Vector{T2}}, gidx::Vector{Vector{Int64}};cellidx=1:size(Y,3),RNG=MersenneTwister(rand(UInt32))) where T <: Real where T2 <: Integer
	_,ntrials,ncells2 = size(Y)
    nbins, _, ncells = size(X)
    tidx = fill(0, ncells2, ntrials)
	flat_labels = cat(labels..., dims=1)
	out_label = fill(0, ntrials)
	for t in 1:ntrials
		#choose a random label
		label = rand(RNG, flat_labels)
		out_label[t] = label
		for (ci,c) in enumerate(cellidx)
			idxs = shuffle(RNG, gidx[c])
			lc = labels[c]
			for _idx in idxs
				if lc[_idx] == label
					Y[:,t,ci] .= X[:,_idx,c]
					tidx[ci,t] = _idx
					break
				end
			end
		end
	end
    tidx, out_label
end

function scramble_trials!(Y::AbstractArray{T,3}, X::AbstractArray{T,3},labels::Vector{Vector{T2}}) where T <: Real where T2
	nbins, ntrials, ncells = size(X)
	size(X) == size(Y) || ArgumentError("`X` and `Y` should have the same size")
	for c in 1:ncells
		_label = unique(labels[c])
		sort!(_label)
		for l in _label
			tidx = findall(labels[c].==l)
			Y[:,shuffle(tidx),c] .= X[:,tidx,c]
		end
	end
end

function run_decoder(X::Array{T,3},label::Vector{Vector{Int64}},testlabel::Vector{Vector{Int64}}=label;numcells=size(X,1),ntrain=1500, ntest=100, nruns=1000, decoder=MultivariateStats.MulticlassLDA, RNG=MersenneTwister(rand(UInt64)),fixedcells=Int64[]) where T <: Real
    ncells, ntrials, nbins = size(X)
    perf = fill(0.0, nbins, nbins, nruns)
	decoders = Vector{Decoders{T,decoder{T}}}(undef, nruns)
	Xp = permutedims(X, [3,2,1])
	nlabel = maximum(cat(label..., dims=1))
	conf_matrix = fill(0.0, nlabel, nlabel, nbins, nbins)
	Y = fill(0.0, nbins, ntrain+ntest, numcells)
	prog = Progress(nruns, 1.0, "Training decoders...")
	progidx = fill(false, nruns)
    Threads.@threads for i in 1:nruns
		if numcells < ncells
            nfixed = length(fixedcells)
            cellidx = shuffle(RNG, setdiff(1:ncells, fixedcells))[1:(numcells-nfixed)]
            append!(cellidx, fixedcells)
			sort!(cellidx)
		else
			cellidx = 1:ncells
		end
		traintestidx = split_trials([collect(1:length(l)) for l in label],RNG=RNG)
		trainidx = [idx[1] for idx in traintestidx]
		testidx = [idx[2] for idx in traintestidx]
		_trainidx, _trainlabel = sample_trials!(view(Y, :, 1:ntrain, :), Xp, label, trainidx,cellidx=cellidx,RNG=RNG)
		_testidx, _testlabel = sample_trials!(view(Y, :, (ntrain+1):(ntrain+ntest), :), Xp, testlabel, testidx, cellidx=cellidx, RNG=RNG)
		dl, decoders[i] = decode(permutedims(Y, [3,2,1]), cat(_trainlabel,_testlabel,dims=1); trainidx=1:ntrain, testidx=(ntrain+1):(ntrain+ntest),decoder=decoder)
        perf[:,:,i] = dropdims(mean(_testlabel.==dl,dims=1),dims=1)
		progidx[i] = true
		pm = maximum(mean(perf[:,:,progidx],dims=3))
		get_confusion_matrix!(conf_matrix, _testlabel, dl)
		next!(prog; showvalues=[(:max_perf, pm)])
    end
    perf, conf_matrix, decoders
end
end #module

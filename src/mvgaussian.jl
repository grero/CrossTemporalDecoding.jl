using StatsBase
import StatsBase.fit
import MultivariateStats.transform

struct GaussianDiscriminant{T<:Real}
	pmeans::Matrix{T}
	Σ::Matrix{T}
end

function StatsBase.fit(::Type{GaussianDiscriminant}, nc::Integer, X::Matrix{T}, class_label::Vector{T2}) where T <: Real where T2
	ndims, ntrials = size(X)
	Σ = cov(X,dims=2)
	label = unique(class_label)
	sort!(label)
	nci = Dict(label[i]=>i for i in 1:nc)
	cmeans = fill(zero(T), ndims, nc)
	trials_per_label = fill(0, 1,nc)
	for i in 1:ntrials
		l = class_label[i]
		c = nci[l]
		cmeans[:,c] .+= X[:,i]
		trials_per_label[c] += 1
	end
	cmeans ./= trials_per_label
	GaussianDiscriminant(cmeans, Σ)
end

function MultivariateStats.transform(gd::GaussianDiscriminant{T}, X::AbstractMatrix{T}) where T <: Real
	X
end



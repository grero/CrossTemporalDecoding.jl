using MultivariateStats:PCA, MulticlassLDA, transform
using StatsBase:fit

abstract type AbstractDecoders end

AllDecoders{T} = Union{MulticlassLDA{T}, GaussianDiscriminant{T}}

struct Decoders{T<:Real, T2<:AllDecoders{T}} <: AbstractDecoders
	pca::PCA{T}
	lda::Vector{T2}
end

"""
Transform a single vector `X`
"""
function MultivariateStats.transform(C::Decoders{T,T2}, X::Vector{T}) where T2 <: AllDecoders{T} where T <: Real
	Y = transform(C.pca, X)
	Z = Vector{Vector{T}}(undef, length(C.lda))
	for (i,lda) in enumerate(C.lda)
		Z[i] = transform(lda, Y)
	end
	Z
end

function MultivariateStats.transform(C::Decoders{T,T2}, X::AbstractArray{T,3}) where T2 <: AllDecoders{T} where T <: Real
	ncells,ntrials,nbins = size(X)
	Z = Vector{Array{T,3}}(undef, length(C.lda))
	for (k,lda) in enumerate(C.lda)
		Z[k] = fill(zero(T), outdim(lda),ntrials,nbins)
		for j in 1:nbins
			for i in 1:ntrials
				Y = transform(C.pca, X[:,i,j])
				Z[k][:,i,j] .= transform(lda, Y)
			end
		end
	end
	Z
end

StatsBase.fit(::Type{MulticlassLDA{T}}, n::Int64, X::AbstractMatrix{T}, y::AbstractVector{Int64};kvs...) where T <: Real = fit(MulticlassLDA, n, X, y;kvs...)
